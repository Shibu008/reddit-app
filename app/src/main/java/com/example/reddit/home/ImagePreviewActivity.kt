package com.example.reddit.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.DrawableCompat
import com.example.reddit.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_image_preview.*
import kotlinx.android.synthetic.main.image_preview_bottom.*


class ImagePreviewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_preview)

        val bundle = intent.getBundleExtra("Bundle")
        val tag = bundle?.getString("FEED_TAG_KEY")
        Log.d("IPA", "$tag")

        tag_text.text = bundle?.getString("FEED_TAG_KEY")
        title_text.text = bundle?.getString("FEED_TITLE_KEY")
        Picasso.get().load(bundle.getString("FEED_IMAGE_KEY")).into(post_image)
        rating_up_text.text = bundle?.getString("FEED_RATING_UP_KEY")
        rating_down_text.text = ""

        back_button.setOnClickListener {
            super.onBackPressed()
        }

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        var drawable = toolbar.overflowIcon
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(drawable.mutate(), resources.getColor(R.color.colorPrimary))
            toolbar.overflowIcon = drawable
        }

        share_text.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)

            intent.type = "text/html"
            intent.putExtra(Intent.EXTRA_TEXT, "${title_text.text} \nwww.reddit.com")

            startActivity(Intent.createChooser(intent, "Share"))
        }
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.image_preview_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.download_icon -> {
                Toast.makeText(applicationContext, "click on setting", Toast.LENGTH_LONG).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}