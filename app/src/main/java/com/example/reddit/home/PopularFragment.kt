package com.example.reddit.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.reddit.R
import com.example.reddit.home.adapter.FeedPostAdapter
import com.example.reddit.home.viewModel.FeedViewModel

class PopularFragment: Fragment() {

    lateinit var feedViewModel: FeedViewModel
    lateinit var postRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_popular, container, false)
        postRecyclerView = view.findViewById(R.id.post_recycler_view)
        return view;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        feedViewModel = ViewModelProvider(this).get(FeedViewModel::class.java)
        feedViewModel.feedData.observe(viewLifecycleOwner, Observer {
            postRecyclerView.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            val adapter = activity?.let { it1 -> FeedPostAdapter(it1, it.data.children) }
            postRecyclerView.adapter = adapter
            //textView.text = it.entryList?.get(0)?.title
            Log.d("Popular", "${it.data.children[0].data.author}")
        } )

    }
}