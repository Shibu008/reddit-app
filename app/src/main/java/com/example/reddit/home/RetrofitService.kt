package com.example.reddit.home

import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory

class RetrofitService {
    private val BASE_URL = "https://www.reddit.com/r/"

    fun <S> createService(serviceClass: Class<S>?): S {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .build()

        return retrofit.create(serviceClass)
    }

}