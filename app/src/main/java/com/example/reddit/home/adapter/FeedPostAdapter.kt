package com.example.reddit.home.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.reddit.R
import com.example.reddit.home.ImagePreviewActivity
import com.example.reddit.home.entity.Children
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_image_preview.*
import org.ocpsoft.prettytime.PrettyTime
import java.util.*

class FeedPostAdapter(private val context: Context, private val feedList: List<Children>): RecyclerView.Adapter<FeedPostAdapter.ViewHolder>() {
    private val EN_LANGUAGE = "en"
    private val TIME_CONVERTER = 1000

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return feedList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.avtarName.text = feedList[position].data.subreddit_name_prefixed
        holder.postContent.text = feedList[position].data.title
        Log.d("Label Url","${feedList[position].data.url}")
        if(feedList[position].data.thumbnail.isNotEmpty()) {
            holder.postImageView.visibility = View.VISIBLE
            Picasso.get().load(feedList[position].data.url_overridden_by_dest).into(holder.postImageView)
        } else {
            holder.postImageView.visibility = View.GONE
        }
        holder.awardsCount.text = "${feedList[position].data.total_awards_received} Awards"
        holder.upRatingTextView.text = numberCalculation(feedList[position].data.ups).toString()
        holder.downRatingTextView.text = numberCalculation(feedList[position].data.downs).toString()

        val timeStamp: Int = feedList[position].data.created
        val time = Date(timeStamp.toLong() * TIME_CONVERTER)
        val pt = PrettyTime(Locale(EN_LANGUAGE))
        holder.postedTextView.text = "Posted by ${feedList[position].data.author} - ${pt.format(time)} - ${feedList[position].data.domain}"

        holder.postImageView.setOnClickListener {
            Log.d("Image Preview ", "Clicked")
            Toast.makeText(context, "Image Clicked", Toast.LENGTH_SHORT).show()
            val bundle = Bundle()
            val intent = Intent(context, ImagePreviewActivity::class.java)
            bundle.putString("FEED_TAG_KEY", "${feedList[position].data.subreddit_name_prefixed} - ${feedList[position].data.author} - ${pt.format(time)}")
            bundle.putString("FEED_TITLE_KEY", feedList[position].data.title)
            bundle.putString("FEED_IMAGE_KEY", feedList[position].data.url_overridden_by_dest)
            bundle.putString("FEED_RATING_UP_KEY", numberCalculation(feedList[position].data.ups).toString())
            intent.putExtra("Bundle", bundle)
            context.startActivity(intent)
        }

        holder.shareTextView.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)

            intent.type = "text/html"
            intent.putExtra(Intent.EXTRA_TEXT, "${feedList[position].data.title} \n www.reddit.com")

            context.startActivity(Intent.createChooser(intent, "Share"))
        }

    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val avtarImageView = itemView.findViewById<ImageView>(R.id.avtarImage)
        val avtarName = itemView.findViewById<TextView>(R.id.avtarName)
        val postedTextView = itemView.findViewById<TextView>(R.id.postedTextView)
        val awardsCount = itemView.findViewById<TextView>(R.id.awardsCount)
        val postContent = itemView.findViewById<TextView>(R.id.post_content)
        val postImageView = itemView.findViewById<ImageView>(R.id.post_image)
        val upRatingTextView = itemView.findViewById<TextView>(R.id.rating_up)
        val downRatingTextView = itemView.findViewById<TextView>(R.id.rating_down)
        val commentRatingTextView = itemView.findViewById<TextView>(R.id.comment_count)
        val shareTextView = itemView.findViewById<TextView>(R.id.share_text)
    }

    private fun numberCalculation(number: Long): String? {
        if (number < 1000) return "" + number
        val exp = (Math.log(number.toDouble()) / Math.log(1000.0)).toInt()
        return String.format(
            "%.1f %c",
            number / Math.pow(1000.0, exp.toDouble()),
            "kMGTPE"[exp - 1]
        )
    }
}