package com.example.reddit.home.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Children(
    val `data`: DataX,
    val kind: String
) : Parcelable