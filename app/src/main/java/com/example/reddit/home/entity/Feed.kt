package com.example.reddit.home.entity

data class Feed(
    val `data`: Data,
    val kind: String
)