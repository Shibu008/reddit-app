package com.example.reddit.home.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Gildings(
    val gid_1: Int,
    val gid_2: Int,
    val gid_3: Int
) : Parcelable