package com.example.reddit.home.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Preview(
    val enabled: Boolean,
    val images: List<Image>
) : Parcelable