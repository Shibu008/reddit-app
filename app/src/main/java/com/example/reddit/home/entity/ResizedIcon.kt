package com.example.reddit.home.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResizedIcon(
    val height: Int,
    val url: String,
    val width: Int
) : Parcelable