package com.example.reddit.home.network

import com.example.reddit.home.entity.Feed
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface FeedApi {

    @GET("/top.json")
    fun getPopularFeeds(
        @Query("after") after: String,
        @Query("limit") limit: String
    ): Call<Feed>

}