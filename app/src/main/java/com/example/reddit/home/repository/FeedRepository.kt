package com.example.reddit.home.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.reddit.home.entity.Feed
import com.example.reddit.home.network.FeedApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory

class FeedRepository(application: Application) {
    private val BASE_URL = "https://www.reddit.com/"
    val feedData = MutableLiveData<Feed>()

    init {
        getAllPopularFeeds()
    }

    private fun getAllPopularFeeds() {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        val service = retrofit.create(FeedApi::class.java)

        service.getPopularFeeds("", "30").enqueue(object : Callback<Feed>{
            override fun onFailure(call: Call<Feed>, t: Throwable) {
                Log.d("Response Failed ",t.message )
            }

            override fun onResponse(call: Call<Feed>, response: Response<Feed>) {
                Log.d("Response Success ", response.body()!!.data.children[0].data.author)
                feedData.value = response.body()
            }

        })
    }
}