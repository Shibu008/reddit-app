package com.example.reddit.home.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.reddit.home.entity.Feed
import com.example.reddit.home.repository.FeedRepository

class FeedViewModel constructor(application: Application): AndroidViewModel(application) {
    private val feedRepository = FeedRepository(application)
    val feedData: LiveData<Feed>

    init {
        feedData = feedRepository.feedData
    }


}